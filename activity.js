db.users.insertMany([
		{
			"firstName": "Stephen",
			"lastName": "Hawking",
			"age": 76,
			"email": "stephenhawking@mail.com",
			"department": "HR"
		},
		{
			"firstName": "Neil",
			"lastName": "Armstrong",
			"age": 82,
			"email": "neilarmstrong@mail.com",
			"department": "HR"
		},
		{
			"firstName": "Bill",
			"lastName": "Gates",
			"age": 65,
			"email": "billgates@mail.com",
			"department": "Operations"
		},
		{
			"firstName": "Jane",
			"lastName": "Doe",
			"age": 82,
			"email": "neilarmstrong@mail.com",
			"department": "HR"
		}

])
// 3.
db.users.find({
				$or: [
						{"firstName": {$regex: "S", $options: '$i'}},
						{"lastName": {$regex: "D", $options: '$i'}}
				]
			},
			{
	"_id": 0,
	"firstName": 1,
	"lastName": 1

})


// 4.

db.users.find({
				$and: [
						{"firstName": {$regex: "E", $options: '$i'}},
						{"age": {$lte: 30}}
				]
			})


			